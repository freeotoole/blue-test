# Technical Test for UI dev position at Fairfax

Instructions for test - https://ffxblue.github.io/interview-tests/test/weather-widget-ui/

While the instructions say focus on the UI and semantic markup before JavaScript, I decided that as my JavaScript skills are un-proven I would spend time on that and rush the css and markup.


## Prerequisites

This has been developed in Vue on macOS with NPM and SASS installed

To install SASS loader and dependancies:
npm install sass-loader node-sass webpack --save-dev

Start dev environment:
npm run serve
